import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { MovieComponent } from './movie.component';
import { FormsModule } from '@angular/forms';
import { MovieCardComponent } from './movie-card/movie-card.component';
import { MovieService } from './services/movie.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MovieModel } from './models/movie.model';

describe('MovieComponent', () => {
  let component: MovieComponent;
  let fixture: ComponentFixture<MovieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieComponent, MovieCardComponent],
      imports: [FormsModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject([MovieService], (movieService: MovieService<MovieModel>) => {
    expect(component).toBeTruthy();
  }));
});
