import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { MovieDetailsComponent } from './movie-details.component';
import { FormsModule } from '@angular/forms';
import { MovieService } from '../services/movie.service';
import { MovieModel } from '../models/movie.model';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { of } from 'rxjs';


class MockActivatedRoute extends ActivatedRoute {
  snapshot: ActivatedRouteSnapshot;
  constructor() {
    super();
    this.snapshot.params = of({ id: 'tt0087538' });
  }
}
/*
class MockActivatedRoute {
  parent = {
    snapshot: {data: {id: 'tt0087538' } },
    routeConfig: { children: { filter: () => {} } }
  };
}*/

describe('MovieDetailsComponent', () => {
  let component: MovieDetailsComponent;
  let fixture: ComponentFixture<MovieDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieDetailsComponent ],
      imports: [FormsModule, HttpClientTestingModule],
      providers: [{ provide: ActivatedRoute, useClass: MockActivatedRoute }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject([MovieService], (movieService: MovieService<MovieModel>) => {
    expect(component).toBeTruthy();
  }));
});
