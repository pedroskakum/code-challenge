import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MovieService } from './movie.service';
import { GenericModel } from '../models/generic.model';
import { MovieModel } from '../models/movie.model';

/*
describe('MovieService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MovieService<GenericModel> = TestBed.get(MovieService);
    expect(service).toBeTruthy();
  });
});
*/

describe('TestMovieService', () => {

  // let movieService: MovieService<GenericModel>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MovieService],
      imports: [HttpClientTestingModule]
    });
    // movieService = TestBed.get(MovieService);
  });

  it('get() should http GET', inject([MovieService], (movieService: MovieService<MovieModel>) => {

    const karatekid = {"Title":"The Karate Kid","Year":"1984","Rated":"PG","Released":"22 Jun 1984","Runtime":"126 min","Genre":"Action, Drama, Family, Sport","Director":"John G. Avildsen","Writer":"Robert Mark Kamen","Actors":"Ralph Macchio, Pat Morita, Elisabeth Shue, Martin Kove","Plot":"A boy and his mother move to California for a new job. He struggles to fit in, as a group of karate students starts to bully him for dating a rich girl from their clique. It's up to the Japanese landlord, Miyagi, to teach him karate.","Language":"English, Japanese","Country":"USA","Awards":"Nominated for 1 Oscar. Another 2 wins & 2 nominations.","Poster":"https://m.media-amazon.com/images/M/MV5BNTkzY2YzNmYtY2ViMS00MThiLWFlYTEtOWQ1OTBiOGEwMTdhXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg","Ratings":[{"Source":"Internet Movie Database","Value":"7.2/10"},{"Source":"Rotten Tomatoes","Value":"88%"},{"Source":"Metacritic","Value":"60/100"}],"Metascore":"60","imdbRating":"7.2","imdbVotes":"161,832","imdbID":"tt0087538","Type":"movie","DVD":"14 Apr 1998","BoxOffice":"N/A","Production":"Columbia Pictures","Website":"N/A","Response":"True"};

    movieService.get({ i: 'tt0087538' }).then(movie => {
      expect(movie).toEqual(karatekid);
    });
  }));
});
